﻿using Application.Interfaces;
using Domain.Entities;
using Domain.Interfaces.Usuarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Apps
{
    public class ApplicationUsuario : IAppUsuario
    {
        InterfaceUsuario _InterfaceUsuario;

        public ApplicationUsuario(InterfaceUsuario InterfaceUsuario)
        {
            _InterfaceUsuario = InterfaceUsuario;
        }

        public void Add(Usuario Entity)
        {
            _InterfaceUsuario.Add(Entity);
        }

        public void Delete(Usuario Entity)
        {
            _InterfaceUsuario.Delete(Entity);
        }

        public Usuario GetEntity(int Id)
        {
            var obj = _InterfaceUsuario.GetEntity(Id);
            return obj;
        }

        public List<Usuario> List()
        {
            var obj = _InterfaceUsuario.List();
            return obj;
        }

        public void Update(Usuario Entity)
        {
            _InterfaceUsuario.Update(Entity);
        }
    }
}
