﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DDD_WEB_CORE.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly IAppUsuario _AppUsuario;
        
        public UsuariosController(IAppUsuario AppUsuario) : base()
        {
            _AppUsuario = AppUsuario;
        }

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(_AppUsuario.List());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int id)
        {
            return View(_AppUsuario.GetEntity(id)); 
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View(new Usuario());
        }

        // POST: Usuarios/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario Usuario)
        {
            try
            {
                // TODO: Add insert logic here
                _AppUsuario.Add(Usuario);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_AppUsuario.GetEntity(id));
        }

        // POST: Usuarios/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Usuario Usuario)
        {
            try
            {
                // TODO: Add update logic here
                _AppUsuario.Update(Usuario);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_AppUsuario.GetEntity(id));
        }

        // POST: Usuarios/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Usuario Usuario)
        {
            try
            {
                // TODO: Add delete logic here
                _AppUsuario.Delete(Usuario);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}