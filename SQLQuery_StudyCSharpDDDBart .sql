-- ####################
-- Inicio - Criar Banco
create database DDDBart
go
use DDDBart
-- Fim - Criar Banco
-- ####################


-- ####################
-- Inicio - Criar Tabela
GO
create table Usuario(
 Id int primary key identity,
NomeUsuario varchar(255)
)
-- Fim - Criar Tabela
-- ####################



go 

create procedure SalvarUsuario
(
@NomeUsuario varchar(255)
)
as
Begin 
Insert into Usuario values(@NomeUsuario)
end

go 

create procedure ListarTodos
as
Begin
select * from Usuario
end

GO

create procedure AtualizarUsuario
(
@NomeUsuario varchar(255),
@Id int
)
as
Begin 
update  Usuario set NomeUsuario = @NomeUsuario where Id = @Id
end

GO

create procedure ExcluirUsuario
(
@Id int
)
as
Begin 
delete from  Usuario  where Id = @Id
end

GO

create procedure ObterPorId
(
@Id int
)
as
Begin 
select * from  Usuario  where Id = @Id
end
