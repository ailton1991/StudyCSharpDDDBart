﻿using Domain.Interfaces.Generic;
using Infra.Config;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Repository.Generic
{
    public class RepositoryGeneric<T> : InterfaceGeneric<T>, IDisposable where T : class
    {
        private readonly DbContextOptionsBuilder<ContextBase> _OptionsBuilder;

        public RepositoryGeneric()
        {
            _OptionsBuilder = new DbContextOptionsBuilder<ContextBase>();
        }

        public void Add(T Entity)
        {
            using (var banco = new ContextBase(_OptionsBuilder.Options))
            {
                banco.Set<T>().Add(Entity);
                banco.SaveChanges();
            }
        }

        public void Delete(T Entity)
        {
            using (var banco = new ContextBase(_OptionsBuilder.Options))
            {
                banco.Set<T>().Remove(Entity);
                banco.SaveChanges();
            }
        }

        public T GetEntity(int Id)
        {
            using (var banco = new ContextBase(_OptionsBuilder.Options))
            {
                var obj = banco.Set<T>().Find(Id);
                return obj;
            }
        }

        public List<T> List()
        {
            using (var banco = new ContextBase(_OptionsBuilder.Options))
            {
                var obj = banco.Set<T>().AsNoTracking().ToList();
                return obj;
            }
        }

        public void Update(T Entity)
        {
            using (var banco = new ContextBase(_OptionsBuilder.Options))
            {
                banco.Set<T>().Update(Entity);
                banco.SaveChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDispose)
        {
            if (!isDispose) return;
        }

        ~RepositoryGeneric()
        {
            Dispose(false);
        }
    }
}
