﻿using Domain.Entities;
using Domain.Interfaces.Usuarios;
using Infra.Repository.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Repository.Usuarios
{
    public class RepositoryUsuarios : RepositoryGeneric<Usuario>, InterfaceUsuario
    {

    }
}
