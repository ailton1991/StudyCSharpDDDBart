﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Config
{
    public class ContextBase : DbContext
    {
        // Contrutor da Classe
        public ContextBase(DbContextOptions<ContextBase> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(StringConnectionConfig());
            }
        }

        public DbSet<Usuario> Usuarios { get; set; }

        private string StringConnectionConfig()
        {
            string strCon = "Server=JUNIOR_ASUS-PC\\SQLEXPRESS;Database=DDDBart;Trusted_Connection=True;MultipleActiveResultSets=True";  // Ailton Xavier

            return strCon;
        }
    }
}
