﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
   [Table("Usuario")]
   public class Usuario
    {
        //Propriedade
        [Column("Id")]
        [Display(Description = "Código")] //FrontEnd da Página
        public int Id { get; set; }

        //Propriedade
        [Column("NomeUsuario")]
        [Display(Description = "Nome do Usuário")] //FrontEnd da Página
        public string NomeUsuario { get; set; }
    }
}
