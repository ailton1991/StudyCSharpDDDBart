﻿using Domain.Entities;
using Domain.Interfaces.Generic;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces.Usuarios
{
    public interface InterfaceUsuario : InterfaceGeneric<Usuario>
    {
    }
}
